# ForumFrontend

A front-end for CASA forum, a virtual teaching assistant project created with angular CLI.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Running 
The backend for this is written in Spring. The repository for the server is [here](https://gitlab.com/umesh-timalsina/casaforumbackend)
