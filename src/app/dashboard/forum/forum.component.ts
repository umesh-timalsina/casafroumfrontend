import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseManagementService} from '../../course.service';
import {CourseForumPostsInfo} from '../../models/courseForumPostsInfo';
import {ForumPostDetail} from '../../models/forumPostDetail';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {
  courseName: object;
  courseId;
  forumPosts: CourseForumPostsInfo[];
  forumPostsDetail: ForumPostDetail;
  @ViewChild('f') messageForm: NgForm;
  constructor(private router: Router, private route: ActivatedRoute,
              private courseManagementService: CourseManagementService) { }

  ngOnInit() {
    this.courseName =  this.route.snapshot.params.coursename;
    this.courseId = this.route.snapshot.params.id;
    this.courseManagementService.getCourseForumPosts(this.courseId).subscribe(
      res => this.forumPosts = res as CourseForumPostsInfo[]
    );
  }
  public clickToViewForumPostDetail(id){
    this.courseManagementService.
    getCourseForumPostsDetail(this.courseId, id).
    subscribe(
      result => this.forumPostsDetail = result as ForumPostDetail
    );
  }

  onCreateNewForumPost() {
    const data = {
      title : this.messageForm.value.title,
      content : this.messageForm.value.content
    };
    this.courseManagementService
      .createANewForumPost(this.courseId, data)
      .subscribe(result => this.courseManagementService
        .getCourseForumPosts(this.courseId).subscribe(
          res => this.forumPosts = res as ForumPostDetail[]
        ));
    this.messageForm.reset();
  }
}
