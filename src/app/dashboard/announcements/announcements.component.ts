import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseManagementService} from '../../course.service';
import {AnnouncementsInfoModel} from '../../models/announcementsInfo.model';
import {AnnouncementDetailModel} from '../../models/announcementDetail.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {
  courseName: object;
  courseId;
  courseAnnoucements;
  announcementDetails: AnnouncementDetailModel;
  canCreateNewAnnouncement: boolean;
  @ViewChild('f') messageForm: NgForm;
  constructor(private router: Router, private route: ActivatedRoute,
              private courseManagementService: CourseManagementService) { }

  ngOnInit() {
    this.courseName =  this.route.snapshot.params.coursename;
    this.courseId = this.route.snapshot.params.id;
    this.courseManagementService.getCourseAnnouncements(this.courseId).subscribe(
      res => this.courseAnnoucements = res as AnnouncementsInfoModel[]
    );
    localStorage.getItem('role').split('|').map(
      val => this.canCreateNewAnnouncement =
        (this.canCreateNewAnnouncement ||  (val === 'ROLE_INSTRUCTOR')));
  }

  clickToViewAnnouncement(id) {
    this.courseManagementService.
        getCourseAnnouncementDetails(this.courseId, id).
            subscribe(
              result => this.announcementDetails = result as AnnouncementDetailModel
    );
  }

  onCreateNewAnnouncement() {
    const data = {
      title : this.messageForm.value.title,
      content : this.messageForm.value.content
    };
    this.courseManagementService
      .createANewAnnouncement(this.courseId, data)
      .subscribe(result => this.courseManagementService
        .getCourseAnnouncements(this.courseId).subscribe(
        res => this.courseAnnoucements = res as AnnouncementsInfoModel[]
      ));
    this.messageForm.reset();
  }


}
