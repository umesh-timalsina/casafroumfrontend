import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseManagementService} from '../../course.service';
import {CourseInfo} from '../../models/courseInfo.model';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  courses: CourseInfo [] = [];
  constructor(private courseManagementService: CourseManagementService,
              private router: Router, private route: ActivatedRoute) { }

  naviageToCourse(id) {
    console.log(id);
    this.router.navigate(['../course', id], {relativeTo: this.route});
  }

  ngOnInit() {
    this.courseManagementService.getCourseList().subscribe(
      result => {
        this.courses = result as CourseInfo [];
        console.log(this.courses);
      }
    );
  }

}
