import { Component, OnInit } from '@angular/core';
import {Course} from '../../models/course.model';
import {CourseManagementService} from '../../course.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  course: Course;
  private id;
  constructor(private courseManagementService: CourseManagementService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.courseManagementService.getCourseDetail(this.id).subscribe(
      result => {
        this.course = result as Course;
        console.log(this.course);
      }
    );
  }

  onClickAnnouncements(){
    this.router.navigate(['../../announcements', this.course.courseName, this.id], {relativeTo: this.route});
  }

  onClickCourseForum(){
    this.router.navigate(['../../forum', this.course.courseName, this.id], {relativeTo: this.route});
  }

}
