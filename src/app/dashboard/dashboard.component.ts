import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  userName: string;
  constructor(
    private router: Router,
    private authService: AuthService
    ) { }

  logout() {
    this.authService.logout();
    console.log('after logout');
    console.log(this.authService.isLoggedIn());
  }

  ngOnInit() {
    this.userName = localStorage.getItem('firstName');
  }



}
