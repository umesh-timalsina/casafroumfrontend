export interface SignupForm {
  fullName: string;
  emailAddress: string;
  userName: string;
  password: string;
  phone: string;
  role: string;
}
