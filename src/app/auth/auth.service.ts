import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private tokenTimer;
  private isAuthenticated = false;
  private jwtToken = null;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  public signup(user) {
    return this.http.post("http://localhost:8080/api/auth/signup", user);
  }

  public login(user) {
    return this.http.post("http://localhost:8080/api/auth/signin", user);
  }

  // public getUser() {
  //   return this.http.get("http://localhost:3600/api/users/users");
  // }

  public getToken() {
    return localStorage.token;
  }

  public isLoggedIn() {
    return localStorage.isAuthenticated === 'true';
  }

  public isAdmin() {
    return localStorage.role === "admin" ? true : false;
  }

  public isStudent() {
    return localStorage.role === "student" ? true : false;
  }

  public logout() {
    this.logoutHelper();
    this.clearAuthData();
    this.router.navigate(["/login"]);
  }

  public logoutHelper() {
    this.jwtToken = null;
    this.isAuthenticated = false;
  }

  public loginSuccess(response) {
    console.log('login success');
    const token = response.jwt;
    const role = response.role;
    const firstName = response.firstName;
    const lastName = response.lastName;
    this.isAuthenticated = true;
    this.jwtToken = token;
    const now = new Date();
    this.saveAuthData(token, role, firstName, lastName);
  }

  private saveAuthData(
    token: string,
    role: string,
    firstName: string,
    lastName: string
  ) {
    localStorage.setItem("token", token);
    localStorage.setItem("isAuthenticated", "true");
    localStorage.setItem("role", role);
    localStorage.setItem("firstName", firstName);
    localStorage.setItem("lastName", lastName);
  }

  private clearAuthData() {
    localStorage.removeItem("token");
    localStorage.removeItem("isAuthenticated");
    localStorage.removeItem("role");
    localStorage.removeItem("firstName");
    localStorage.removeItem("lastName");
  }

  private getAuthData() {
    const token = localStorage.getItem("token");
    const role = localStorage.getItem("role");
    const expirationDate = localStorage.getItem("expiration");
    if (!token || !role || !expirationDate) {
      return;
    }
    return {
      token: token,
      role: role,
      expirationDate: new Date(expirationDate)
    };
  }


}



