import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CoursesComponent} from './dashboard/courses/courses.component';
import {CourseComponent} from './dashboard/course/course.component';
import { AuthGuard } from './auth/guards/auth.guard';
import {AnnouncementsComponent} from './dashboard/announcements/announcements.component';
import {ForumComponent} from './dashboard/forum/forum.component';



const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard],
  children: [
      {path: 'courses', component: CoursesComponent},
      {path: 'course', component: CourseComponent},
      {path: 'course/:id', component: CourseComponent},
      {path: 'announcements/:coursename/:id', component: AnnouncementsComponent},
      {path: 'forum/:coursename/:id', component: ForumComponent}
  ]}
]

@NgModule({
  imports : [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRouting {

}
