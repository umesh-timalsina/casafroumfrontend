export interface AnnouncementDetailModel {
  canEdit: true;
  content: string;
  createdAt;
  id: 0;
  postedBy;
  title: string;
  updatedAt;

}
