export interface ForumPostDetail {
  canEdit;
  createdAt;
  id;
  postBody;
  postTitle;
  postedBy;
  updatedAt;

}
