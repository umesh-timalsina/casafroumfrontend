export interface CourseForumPostsInfo {
    createdAt;
    id;
    postTitle;
}
