export class CourseInfo {
  id: number;
  courseCode: string;
  courseName: boolean;

  constructor(id, courseCode, courseName){
    this.id = id;
    this.courseCode = courseCode;
    this.courseName = courseName;
  }
}
