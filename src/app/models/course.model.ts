export interface Course {
  courseCode: string;
  courseDescription: string;
  courseName: string;
  courseTerm: string;
  id: 0;
}
