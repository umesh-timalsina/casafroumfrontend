import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CourseInfo} from './models/courseInfo.model';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CourseManagementService {
  private baseUrl: string = 'http://localhost:8080';
  private courseInfo: CourseInfo [] = [];

  constructor(private httpService: HttpClient) {}

  public getCourseList(){
    const endPoint = `${this.baseUrl}/api/course`;
    return this.httpService.get(endPoint);
  }

  public getCourseDetail(id) {
    const endPoint = `${this.baseUrl}/api/course/${id}`;
    return this.httpService.get(endPoint);
  }

  public getCourseAnnouncements(courseId) {
    const endPoint = `${this.baseUrl}/api/course/${courseId}/announcements`;
    return this.httpService.get(endPoint);
  }

  public getCourseAnnouncementDetails(courseId, announcementId) {
    const endPoint = `${this.baseUrl}/api/course/${courseId}/announcements/${announcementId}`;
    return this.httpService.get(endPoint);
  }

  public createANewAnnouncement(courseId, data){
    const endPoint = `${this.baseUrl}/api/course/${courseId}/announcements`;
    return this.httpService.post(endPoint, data);
  }

  public getCourseForumPosts(courseId){
    const endPoint = `${this.baseUrl}/api/course/${courseId}/forum`;
    return this.httpService.get(endPoint);

  }

  getCourseForumPostsDetail(courseId, id){
    const endPoint = `${this.baseUrl}/api/course/${courseId}/forum/${id}`;
    return this.httpService.get(endPoint);

  }

  public createANewForumPost(courseId, data){
    const endPoint = `${this.baseUrl}/api/course/${courseId}/forum`;
    return this.httpService.post(endPoint, data);
  }
}
